PdfPageCount
============

Benchmark de várias linguagens. O programa informa o número de páginas dos
arquivos PDF passados na linha de comando. Para isso, a implementação em todas
as linguagens é feita da forma mais semelhante possível.

Exemplo de execução com PowerShell:

    PS C:\> cd \PDFs
    PS C:\PDFs> Set-ExecutionPolicy Unrestricted -Scope Process
    PS C:\PDFs> . .\TestPdfPageCount.ps1
    PS C:\PDFs> run_for_all_pdfs_one_call java_pdf
