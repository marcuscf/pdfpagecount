#!/bin/sh

# Exemplo-guia para compilação.
# Provavelmente precisará ser editado para adequar-se ao seu caso.
# Pode servir como base para um Makefile.

# gcc:
COMPILER=g++
FLAGS="-std=c++14 -Wall -Wextra -O2"
#SANITIZE="-fsanitize=address -fsanitize=undefined"

# When using sanitizers, run with
# LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0 ./PdfPageCount <file-name.pdf>

# mingw:
#COMPILER="i686-w64-mingw32-g++"
#FLAGS="-static -std=c++14 -Wall -Wextra -O2"

# clang:
#COMPILER=clang++-3.6
#FLAGS="-std=c++14 -stdlib=libc++ -Wall -O2"
#LDFLAGS="-nodefaultlibs -lc++ -lc++abi -lm -lc -lgcc_s -lgcc"

${COMPILER} ${FLAGS} PdfPageCount.cpp -o PdfPageCount ${LDFLAGS}
