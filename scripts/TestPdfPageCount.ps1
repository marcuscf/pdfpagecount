$java_exe_path = 'C:\Program Files\Java\jdk1.6.0_21\bin\java.exe'
$java_class_path = '<preencha-aqui>'

$python_exe_path = 'C:\Python31\python.exe'
$python_py_path = '<preencha-aqui>\PdfPageCount.pyc'

$d_path = '<preencha-aqui>\PdfPageCount.exe'
$cxx_path = '<preencha-aqui>\PdfPageCount.exe'
$fsharp_path = '<preencha-aqui>\PdfPageCount.exe'

function java_pdf {
    &$java_exe_path -cp $java_class_path PdfPageCount @args
}

function python_pdf {
    &$python_exe_path $python_py_path @args
}

function d_pdf {
    &$d_path @args
}

function cxx_pdf {
    &$cxx_path @args
}

function fsharp_pdf {
    &$fsharp_path @args
}

function get_pdf_list {
    ls -filter *.pdf -recurse | foreach { $_.fullname }
}

function run_for_all_pdfs_multiple_calls($cmd) {
    $all_pdfs = get_pdf_list
    measure-command { $all_pdfs | foreach { &$cmd $_ | out-host } }
}

function run_for_all_pdfs_one_call($cmd) {
    $all_pdfs = get_pdf_list
    measure-command { &$cmd @all_pdfs | out-host }
}
